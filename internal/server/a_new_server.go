package server

import (
	"context"
	"fmt"
	"log/slog"
	"net"
	"time"

	"budget/internal/dto"

	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/logging"
	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/recovery"
	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const startAppYear = 2024

type Budget interface {
	AddNewCost(ctx context.Context, cost dto.CostRequest) (int64, error)
	AddNewIncome(ctx context.Context, income dto.IncomeRequest) (int64, error)
	DeleteCost(ctx context.Context, userID int64, costID int64) (bool, error)
	DeleteIncome(ctx context.Context, userID int64, incomeID int64) (bool, error)
	ListCostsByDate(ctx context.Context, userID int64, date time.Time) ([]dto.CostView, error)
	ListIncomesByDate(ctx context.Context, userID int64, date time.Time) ([]dto.IncomeView, error)
	WeekListCosts(ctx context.Context, userID int64) ([]dto.SumCosts, error)
	MonthListCosts(ctx context.Context, userID int64, month int32) ([]dto.SumCosts, error)
	MonthListIncomes(ctx context.Context, userID int64, month int32) ([]dto.SumIncomes, error)
	YearListCosts(ctx context.Context, userID int64, year int32) ([]dto.SumCosts, error)
	YearListIncomes(ctx context.Context, userID int64, year int32) ([]dto.SumIncomes, error)
}

type Server struct {
	log        *slog.Logger
	gRPCServer *grpc.Server
	port       int
}

func New(log *slog.Logger, port int, budget Budget) *Server {
	loggingOpts := []logging.Option{
		logging.WithLogOnEvents(
			//logging.StartCall, logging.FinishCall,
			logging.PayloadReceived, logging.PayloadSent,
		),
		// Add any other option (check functions starting with logging.With).
	}

	recoveryOpts := []recovery.Option{
		recovery.WithRecoveryHandler(func(p interface{}) (err error) {
			log.Error("Recovered from panic", slog.Any("panic", p))

			return status.Errorf(codes.Internal, "internal error")
		}),
	}

	gRPCServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		recovery.UnaryServerInterceptor(recoveryOpts...),
		logging.UnaryServerInterceptor(interceptorLogger(log), loggingOpts...),
	))

	register(gRPCServer, budget)

	return &Server{
		log:        log,
		gRPCServer: gRPCServer,
		port:       port,
	}
}

func (s *Server) MustStart() {
	const oper = "server.Start"

	log := s.log.With(slog.String("oper", oper))

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", s.port))
	if err != nil {
		panic(err)
	}

	log.Info("grpc server started", slog.String("addr", l.Addr().String()))

	if err = s.gRPCServer.Serve(l); err != nil {
		panic(err)
	}
}

func (s *Server) Stop() {
	const oper = "server.Stop"

	s.log.With(slog.String("oper", oper)).
		Info("grpc server stopped", slog.Int("port", s.port))

	s.gRPCServer.GracefulStop()
}

// InterceptorLogger adapts slog logger to interceptor logger.
// This code is simple enough to be copied and not imported.
func interceptorLogger(l *slog.Logger) logging.Logger {
	return logging.LoggerFunc(func(ctx context.Context, lvl logging.Level, msg string, fields ...any) {
		l.Log(ctx, slog.Level(lvl), msg, fields...)
	})
}

type handler struct {
	proto.UnimplementedBudgetServer
	budget Budget
}

func register(gRPC *grpc.Server, budget Budget) {
	proto.RegisterBudgetServer(gRPC, &handler{budget: budget})
}
