package server

import (
	"context"
	"errors"
	"time"

	"budget/internal/service/budget"

	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (h *handler) MonthListCost(ctx context.Context, req *proto.MonthListCostsRequest) (*proto.MonthListCostsResponse, error) {
	userID := req.GetUserId()
	if userID == 0 {
		return nil, status.Error(codes.Unauthenticated, "need user id")
	}

	month := req.GetMonth()
	currMonth := int32(time.Now().Month())
	if month == 0 {
		month = currMonth
	}
	if currMonth < month {
		return nil, status.Error(codes.InvalidArgument, "month older than current month")
	}

	listCost, err := h.budget.MonthListCosts(ctx, userID, month)
	if err != nil {
		if errors.Is(err, budget.ErrRecordNotFound) {
			return nil, status.Error(codes.InvalidArgument, "record not found")
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	protoCosts := make([]*proto.SumCost, len(listCost))

	for i, cost := range listCost {
		protoCosts[i] = &proto.SumCost{}
		protoCosts[i].Amount = cost.Amount
		protoCosts[i].Category = cost.Category
	}

	return &proto.MonthListCostsResponse{
		SumCosts: protoCosts,
	}, nil
}
