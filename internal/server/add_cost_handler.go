package server

import (
	"context"
	"time"

	"budget/internal/dto"

	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (h *handler) AddCost(ctx context.Context, req *proto.AddCostRequest) (*proto.AddCostResponse, error) {
	userID := req.GetUserId()
	if userID == 0 {
		return nil, status.Error(codes.Unauthenticated, "missing user_id")
	}

	amount := req.GetAmount()
	if amount <= 0 {
		return nil, status.Error(codes.InvalidArgument, "negative or empty amount")
	}

	category := req.GetCategory()
	if req.GetCategory() == "" {
		return nil, status.Error(codes.InvalidArgument, "missing category")
	}

	date := req.GetCreatedAt().AsTime()
	if date.Equal(time.Unix(0, 0)) || date.IsZero() {
		date = time.Now()
	}

	cost := dto.CostRequest{
		UserID:    userID,
		Amount:    amount,
		Category:  category,
		CreatedAt: date,
		Comment:   req.GetComment(),
	}

	costID, err := h.budget.AddNewCost(ctx, cost)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.AddCostResponse{
		CostId: costID,
	}, nil
}
