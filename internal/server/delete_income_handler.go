package server

import (
	"context"
	"errors"

	"budget/internal/service/budget"

	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (h *handler) DeleteIncome(ctx context.Context, req *proto.DeleteIncomeRequest) (*proto.DeleteIncomeResponse, error) {
	userID := req.GetUserId()
	if userID == 0 {
		return nil, status.Error(codes.Unauthenticated, "need user id")
	}

	incomeID := req.GetIncomeId()
	if incomeID == 0 {
		return nil, status.Error(codes.InvalidArgument, "need id of record")
	}

	isDeleted, err := h.budget.DeleteIncome(ctx, userID, incomeID)
	if err != nil {
		if errors.Is(err, budget.ErrRecordNotFound) {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &proto.DeleteIncomeResponse{
		IsDeleted: isDeleted,
	}, nil
}
