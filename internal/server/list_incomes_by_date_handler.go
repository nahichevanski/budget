package server

import (
	"context"
	"errors"
	"time"

	"budget/internal/service/budget"

	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (h *handler) ListIncomesByDate(ctx context.Context, req *proto.ListIncomesByDateRequest) (*proto.ListIncomesByDateResponse, error) {
	userID := req.GetUserId()
	if userID == 0 {
		return nil, status.Error(codes.Unauthenticated, "need user id")
	}

	date, err := time.Parse("2006-01-02", req.GetDate())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	now := time.Now()
	if date.After(now) {
		return nil, status.Error(codes.InvalidArgument, "Сегодня в завтрашний день не все могут смотреть. Вернее смотреть могут не только лишь все, мало кто может это делать")
	}

	if date.Equal(time.Unix(0, 0)) || date.IsZero() {
		date = now
	}

	incomes, err := h.budget.ListIncomesByDate(ctx, userID, date)
	if err != nil {
		if errors.Is(err, budget.ErrRecordNotFound) {
			return nil, status.Error(codes.InvalidArgument, "record not found")
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	protoIncomes := make([]*proto.Income, len(incomes))

	for i, income := range incomes {
		protoIncomes[i] = &proto.Income{}
		protoIncomes[i].Id = income.ID
		protoIncomes[i].Amount = income.Amount
		protoIncomes[i].Category = income.Category
		protoIncomes[i].Comment = income.Comment
	}

	return &proto.ListIncomesByDateResponse{
		ListIncomes: protoIncomes,
	}, nil
}
