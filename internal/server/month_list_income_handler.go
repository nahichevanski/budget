package server

import (
	"context"
	"errors"
	"time"

	"budget/internal/service/budget"

	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (h *handler) MonthListIncome(ctx context.Context, req *proto.MonthListIncomesRequest) (*proto.MonthListIncomesResponse, error) {
	userID := req.GetUserId()
	if userID == 0 {
		return nil, status.Error(codes.Unauthenticated, "need user id")
	}

	month := req.GetMonth()
	currMonth := int32(time.Now().Month())
	if month == 0 {
		month = currMonth
	}
	if currMonth < month {
		return nil, status.Error(codes.InvalidArgument, "month older than current month")
	}

	listIncomes, err := h.budget.MonthListIncomes(ctx, userID, month)
	if err != nil {
		if errors.Is(err, budget.ErrRecordNotFound) {
			return nil, status.Error(codes.InvalidArgument, "record not found")
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	protoIncomes := make([]*proto.SumIncome, len(listIncomes))

	for i, income := range listIncomes {
		protoIncomes[i] = &proto.SumIncome{}
		protoIncomes[i].Amount = income.Amount
		protoIncomes[i].Category = income.Category
	}

	return &proto.MonthListIncomesResponse{
		SumIncomes: protoIncomes,
	}, nil
}
