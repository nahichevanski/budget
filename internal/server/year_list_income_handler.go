package server

import (
	"context"
	"errors"
	"time"

	"budget/internal/service/budget"

	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (h *handler) YearListIncome(ctx context.Context, req *proto.YearListIncomesRequest) (*proto.YearListIncomesResponse, error) {
	userID := req.GetUserId()
	if userID == 0 {
		return nil, status.Error(codes.Unauthenticated, "need user id")
	}

	year := req.GetYear()
	if currYear := time.Now().Year(); int32(currYear) < year || year < startAppYear {
		return nil, status.Error(codes.InvalidArgument, "year incorrect")
	}

	listIncomes, err := h.budget.YearListIncomes(ctx, userID, year)
	if err != nil {
		if errors.Is(err, budget.ErrRecordNotFound) {
			return nil, status.Error(codes.InvalidArgument, "record not found")
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	protoIncomes := make([]*proto.SumIncome, len(listIncomes))

	for i, income := range listIncomes {
		protoIncomes[i] = &proto.SumIncome{}
		protoIncomes[i].Amount = income.Amount
		protoIncomes[i].Category = income.Category
	}

	return &proto.YearListIncomesResponse{
		SumIncomes: protoIncomes,
	}, nil
}
