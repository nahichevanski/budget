package server

import (
	"context"
	"errors"

	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
)

func (h *handler) DayListCost(ctx context.Context, req *proto.DayListCostsRequest) (*proto.DayListCostsResponse, error) {
	return &proto.DayListCostsResponse{
		SumCosts: nil,
	}, errors.New("not implemented")
}
