package server

import (
	"context"
	"errors"
	"time"

	"budget/internal/service/budget"

	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (h *handler) YearListCost(ctx context.Context, req *proto.YearListCostsRequest) (*proto.YearListCostsResponse, error) {
	userID := req.GetUserId()
	if userID == 0 {
		return nil, status.Error(codes.Unauthenticated, "need user id")
	}

	year := req.GetYear()
	if currYear := time.Now().Year(); int32(currYear) < year || year < startAppYear {
		return nil, status.Error(codes.InvalidArgument, "year incorrect")
	}

	listCost, err := h.budget.YearListCosts(ctx, userID, year)
	if err != nil {
		if errors.Is(err, budget.ErrRecordNotFound) {
			return nil, status.Error(codes.InvalidArgument, "record not found")
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	protoCosts := make([]*proto.SumCost, len(listCost))

	for i, cost := range listCost {
		protoCosts[i] = &proto.SumCost{}
		protoCosts[i].Amount = cost.Amount
		protoCosts[i].Category = cost.Category
	}

	return &proto.YearListCostsResponse{
		SumCosts: protoCosts,
	}, nil
}
