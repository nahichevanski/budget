package server

import (
	"context"
	"errors"

	"budget/internal/service/budget"

	proto "gitlab.com/nahichevanski/proto-budget/gen/go/budget"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (h *handler) WeekListCost(ctx context.Context, req *proto.WeekListCostsRequest) (*proto.WeekListCostsResponse, error) {
	userID := req.GetUserId()
	if userID == 0 {
		return nil, status.Error(codes.Unauthenticated, "need user id")
	}

	listCost, err := h.budget.WeekListCosts(ctx, userID)
	if err != nil {
		if errors.Is(err, budget.ErrRecordNotFound) {
			return nil, status.Error(codes.InvalidArgument, "records not found")
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	protoCosts := make([]*proto.SumCost, len(listCost))

	for i, cost := range listCost {
		protoCosts[i] = &proto.SumCost{}
		protoCosts[i].Amount = cost.Amount
		protoCosts[i].Category = cost.Category
	}

	return &proto.WeekListCostsResponse{
		SumCosts: protoCosts,
	}, nil
}
