package postgres

import (
	"context"
	"errors"
	"fmt"

	"budget/internal/dto"
	"budget/internal/storage"

	"github.com/jackc/pgx/v5"
)

func (s *Storage) SelectWeekListCosts(ctx context.Context, userID int64) ([]dto.SumCosts, error) {
	const oper = "storage.postgres.SelectWeekListCosts"

	costs := make([]dto.SumCosts, 0, 8)

	query := `
		SELECT sum(costs.amount) AS amount, cost_categories.name AS category
		FROM costs
			JOIN cost_categories ON cost_categories.id = costs.category_id
		WHERE costs.user_id = $1
		  	AND DATE_PART('year', costs.created_at) = DATE_PART('year', CURRENT_DATE)
			AND DATE_PART('week', costs.created_at) = DATE_PART('week', CURRENT_DATE)
		GROUP BY category;
	`
	rows, err := s.db.Query(ctx, query, userID)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrRecordNotFound
		}
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	for rows.Next() {
		var cost dto.SumCosts
		if err = rows.Scan(&cost.Amount, &cost.Category); err != nil {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		costs = append(costs, cost)
	}

	return costs, nil
}
