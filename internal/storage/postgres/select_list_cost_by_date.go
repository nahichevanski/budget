package postgres

import (
	"context"
	"errors"
	"fmt"
	"time"

	"budget/internal/dto"
	"budget/internal/storage"

	"github.com/jackc/pgx/v5"
)

func (s *Storage) SelectListCostsByDate(ctx context.Context, userID int64, date time.Time) ([]dto.CostView, error) {
	const oper = "storage.postgres.SelectListCostsByDate"

	costs := make([]dto.CostView, 0, 8)

	query := `
		SELECT
			costs.id,
			costs.amount,
			cost_categories.name AS category,
			costs.comment
		FROM costs
			JOIN cost_categories ON costs.category_id = cost_categories.id
		WHERE costs.user_id = $1 AND costs.created_at = $2
	`
	rows, err := s.db.Query(ctx, query, userID, date)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrRecordNotFound
		}
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	for rows.Next() {
		var cost dto.CostView
		if err = rows.Scan(&cost.ID, &cost.Amount, &cost.Category, &cost.Comment); err != nil {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		costs = append(costs, cost)
	}

	return costs, nil
}
