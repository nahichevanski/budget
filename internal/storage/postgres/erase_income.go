package postgres

import (
	"context"
	"fmt"

	"budget/internal/storage"
)

func (s *Storage) EraseIncome(ctx context.Context, userID, incomeID int64) (bool, error) {
	const oper = "storage.postgres.EraseIncome"

	query := `
	DELETE FROM incomes
	WHERE user_id = $1 AND id = $2
	`
	rows, err := s.db.Exec(ctx, query, userID, incomeID)
	if err != nil {
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	if rows.RowsAffected() == 0 {
		return false, storage.ErrRecordNotFound
	}

	return true, nil
}
