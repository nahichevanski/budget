package postgres

import (
	"context"
	"fmt"

	"budget/internal/storage"
)

func (s *Storage) EraseCost(ctx context.Context, userID, costID int64) (bool, error) {
	const oper = "storage.postgres.EraseCost"

	query := `
	DELETE FROM costs
	WHERE user_id = $1 AND id = $2
	`
	rows, err := s.db.Exec(ctx, query, userID, costID)
	if err != nil {
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	if rows.RowsAffected() == 0 {
		return false, storage.ErrRecordNotFound
	}

	return true, nil
}
