package postgres

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"time"

	"budget/internal/config"
	"budget/internal/lib/sl"

	"github.com/jackc/pgx/v5/pgxpool"
)

type Storage struct {
	db *pgxpool.Pool
}

func New(log *slog.Logger, cfg *config.Config) (*Storage, error) {
	const oper = "storage.postgres.New"

	ctx := context.Background()

	conn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.DB.User, os.Getenv("POSTGRES_PASSWORD"), cfg.DB.Host, cfg.DB.Port, cfg.DB.Name)

	connectConfig, err := pgxpool.ParseConfig(conn)
	if err != nil {
		log.Error("parse config error", sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	connectConfig.ConnConfig.ConnectTimeout = 5 * time.Second

	var db *pgxpool.Pool

	for i := 0; i < 10; i++ {
		db, err = pgxpool.NewWithConfig(ctx, connectConfig)
		if err != nil {
			time.Sleep(time.Duration(i) * 100 * time.Millisecond)
		} else {
			break
		}
	}
	if err != nil {
		log.Error("postgres connection error", sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	log.Info("postgres connection success")
	return &Storage{db: db}, nil
}

func (s *Storage) Close() {
	s.db.Close()
}
