package postgres

import (
	"context"
	"errors"
	"fmt"
	"time"

	"budget/internal/dto"
	"budget/internal/storage"

	"github.com/jackc/pgx/v5"
)

func (s *Storage) SelectListIncomesByDate(ctx context.Context, userID int64, date time.Time) ([]dto.IncomeView, error) {
	const oper = "storage.postgres.SelectListIncomesByDate"

	incomes := make([]dto.IncomeView, 0, 8)

	query := `
		SELECT
			incomes.id,
			incomes.amount,
			income_categories.name AS category,
			incomes.comment
		FROM incomes
			JOIN income_categories ON incomes.category_id = income_categories.id
		WHERE incomes.user_id = $1 AND incomes.created_at = $2
	`
	rows, err := s.db.Query(ctx, query, userID, date)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrRecordNotFound
		}
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	for rows.Next() {
		var income dto.IncomeView
		if err = rows.Scan(&income.ID, &income.Amount, &income.Category, &income.Comment); err != nil {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		incomes = append(incomes, income)
	}

	return incomes, nil
}
