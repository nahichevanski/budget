package postgres

import (
	"context"
	"errors"
	"fmt"

	"budget/internal/dto"
	"budget/internal/storage"

	"github.com/jackc/pgx/v5"
)

func (s *Storage) SelectYearListIncomes(ctx context.Context, userID int64, year int32) ([]dto.SumIncomes, error) {
	const oper = "storage.postgres.SelectYearListIncomes"

	incomes := make([]dto.SumIncomes, 0, 8)

	query := `
		SELECT sum(incomes.amount) AS amount, income_categories.name AS category
		FROM incomes
			JOIN income_categories ON income_categories.id  =  incomes.category_id
		WHERE user_id = $1
			AND DATE_PART('year', created_at) = $2
		GROUP BY category;
	`
	rows, err := s.db.Query(ctx, query, userID, year)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, storage.ErrRecordNotFound
		}
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	for rows.Next() {
		var income dto.SumIncomes
		if err = rows.Scan(&income.Amount, &income.Category); err != nil {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		incomes = append(incomes, income)
	}

	return incomes, nil
}
