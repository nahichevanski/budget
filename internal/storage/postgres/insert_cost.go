package postgres

import (
	"context"
	"fmt"

	"budget/internal/dto"
)

func (s *Storage) InsertCost(ctx context.Context, rec dto.CostRequest) (int64, error) {
	const oper = "storage.postgres.InsertCost"

	query := `
	INSERT INTO costs 
    (user_id,
    category_id,
    amount,
    created_at,
    comment)
    VALUES ($1, (SELECT id FROM cost_categories WHERE name = $2), $3, $4, $5) RETURNING id`

	var id int64

	err := s.db.QueryRow(
		ctx, query, rec.UserID, rec.Category, rec.Amount, rec.CreatedAt, rec.Comment,
	).Scan(&id)
	if err != nil {
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return id, nil
}
