package app

import (
	"log/slog"

	"budget/internal/config"
	"budget/internal/server"
	"budget/internal/service/budget"
	"budget/internal/storage/postgres"
)

type App struct {
	GRPCServer *server.Server
	Storage    *postgres.Storage
}

func New(log *slog.Logger, cfg *config.Config) *App {
	storage, err := postgres.New(log, cfg)
	if err != nil {
		panic(err)
	}

	service := budget.New(log, storage, storage, storage)

	srv := server.New(log, cfg.GRPC.Port, service)

	return &App{
		GRPCServer: srv,
		Storage:    storage,
	}
}
