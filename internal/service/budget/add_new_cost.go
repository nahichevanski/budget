package budget

import (
	"context"
	"fmt"

	"budget/internal/dto"
	"budget/internal/lib/sl"
)

func (b *Budget) AddNewCost(ctx context.Context, cost dto.CostRequest) (int64, error) {
	const oper = "service.budget.AddNewCost"

	costID, err := b.saver.InsertCost(ctx, cost)
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return costID, nil
}
