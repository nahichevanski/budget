package budget

import (
	"context"
	"errors"
	"fmt"
	"time"

	"budget/internal/dto"
	"budget/internal/lib/sl"
	"budget/internal/storage"
)

func (b *Budget) ListIncomesByDate(ctx context.Context, userID int64, date time.Time) ([]dto.IncomeView, error) {
	const oper = "service.budget.ListIncomesByDate"

	listIncomes, err := b.informer.SelectListIncomesByDate(ctx, userID, date)
	if err != nil {
		if errors.Is(err, storage.ErrRecordNotFound) {
			return nil, fmt.Errorf("%s: %w", oper, err)
		}
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return listIncomes, nil
}
