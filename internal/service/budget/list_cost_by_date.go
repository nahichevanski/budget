package budget

import (
	"budget/internal/dto"
	"context"
	"errors"
	"fmt"
	"time"

	"budget/internal/lib/sl"
	"budget/internal/storage"
)

func (b *Budget) ListCostsByDate(ctx context.Context, userID int64, date time.Time) ([]dto.CostView, error) {
	const oper = "service.budget.ListCostsByDate"

	listCost, err := b.informer.SelectListCostsByDate(ctx, userID, date)
	if err != nil {
		if errors.Is(err, storage.ErrRecordNotFound) {
			return nil, ErrRecordNotFound
		}
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return listCost, nil
}
