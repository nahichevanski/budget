package budget

import (
	"context"
	"errors"
	"fmt"

	"budget/internal/dto"
	"budget/internal/lib/sl"
	"budget/internal/storage"
)

func (b *Budget) YearListIncomes(ctx context.Context, userID int64, year int32) ([]dto.SumIncomes, error) {
	const oper = "service.budget.YearListIncomes"

	listIncome, err := b.informer.SelectYearListIncomes(ctx, userID, year)
	if err != nil {
		if errors.Is(err, storage.ErrRecordNotFound) {
			return nil, ErrRecordNotFound
		}
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return listIncome, nil
}
