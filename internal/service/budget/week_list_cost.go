package budget

import (
	"context"
	"errors"
	"fmt"

	"budget/internal/dto"
	"budget/internal/lib/sl"
	"budget/internal/storage"
)

func (b *Budget) WeekListCosts(ctx context.Context, userID int64) ([]dto.SumCosts, error) {
	const oper = "service.budget.WeekListCosts"

	listCost, err := b.informer.SelectWeekListCosts(ctx, userID)
	if err != nil {
		if errors.Is(err, storage.ErrRecordNotFound) {
			return nil, ErrRecordNotFound
		}
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return listCost, nil
}
