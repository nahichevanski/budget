package budget

import (
	"context"
	"errors"
	"fmt"

	"budget/internal/lib/sl"
	"budget/internal/storage"
)

func (b *Budget) DeleteCost(ctx context.Context, userID int64, costID int64) (bool, error) {
	const oper = "service.budget.DeleteCost"

	isDeleted, err := b.eraser.EraseCost(ctx, userID, costID)
	if err != nil {
		if errors.Is(err, storage.ErrRecordNotFound) {
			return false, fmt.Errorf("%s: %w", oper, ErrRecordNotFound)
		}
		b.log.Error(oper, sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return isDeleted, nil
}
