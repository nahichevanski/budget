package budget

import (
	"context"
	"errors"
	"log/slog"
	"time"

	"budget/internal/dto"
)

type Budget struct {
	log      *slog.Logger
	saver    RecordSaver
	eraser   RecordEraser
	informer Informer
}

type RecordSaver interface {
	InsertCost(ctx context.Context, cost dto.CostRequest) (int64, error)
	InsertIncome(ctx context.Context, income dto.IncomeRequest) (int64, error)
}

type RecordEraser interface {
	EraseCost(ctx context.Context, userID, costID int64) (bool, error)
	EraseIncome(ctx context.Context, userID, incomeID int64) (bool, error)
}

type Informer interface {
	SelectListCostsByDate(ctx context.Context, userID int64, date time.Time) ([]dto.CostView, error)
	SelectListIncomesByDate(ctx context.Context, userID int64, date time.Time) ([]dto.IncomeView, error)
	SelectWeekListCosts(ctx context.Context, userID int64) ([]dto.SumCosts, error)
	SelectMonthListCosts(ctx context.Context, userID int64, month int32) ([]dto.SumCosts, error)
	SelectMonthListIncomes(ctx context.Context, userID int64, month int32) ([]dto.SumIncomes, error)
	SelectYearListCosts(ctx context.Context, userID int64, year int32) ([]dto.SumCosts, error)
	SelectYearListIncomes(ctx context.Context, userID int64, year int32) ([]dto.SumIncomes, error)
}

var ErrRecordNotFound = errors.New("record not found")

func New(log *slog.Logger, saver RecordSaver, eraser RecordEraser, informer Informer) *Budget {
	return &Budget{log: log, saver: saver, eraser: eraser, informer: informer}
}
