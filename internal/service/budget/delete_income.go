package budget

import (
	"context"
	"errors"
	"fmt"

	"budget/internal/lib/sl"
	"budget/internal/storage"
)

func (b *Budget) DeleteIncome(ctx context.Context, userID int64, incomeID int64) (bool, error) {
	const oper = "service.budget.DeleteIncome"

	isDeleted, err := b.eraser.EraseIncome(ctx, userID, incomeID)
	if err != nil {
		if errors.Is(err, storage.ErrRecordNotFound) {
			return false, fmt.Errorf("%s: %w", oper, ErrRecordNotFound)
		}
		b.log.Error(oper, sl.Err(err))
		return false, fmt.Errorf("%s: %w", oper, err)
	}

	return isDeleted, nil
}
