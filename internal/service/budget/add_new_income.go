package budget

import (
	"context"
	"fmt"

	"budget/internal/dto"
	"budget/internal/lib/sl"
)

func (b *Budget) AddNewIncome(ctx context.Context, income dto.IncomeRequest) (int64, error) {
	const oper = "service.budget.AddNewIncome"

	incomeID, err := b.saver.InsertIncome(ctx, income)
	if err != nil {
		b.log.Error(oper, sl.Err(err))
		return 0, fmt.Errorf("%s: %w", oper, err)
	}

	return incomeID, nil
}
