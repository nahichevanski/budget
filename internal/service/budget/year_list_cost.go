package budget

import (
	"budget/internal/dto"
	"context"
	"errors"
	"fmt"

	"budget/internal/lib/sl"
	"budget/internal/storage"
)

func (b *Budget) YearListCosts(ctx context.Context, userID int64, year int32) ([]dto.SumCosts, error) {
	const oper = "service.budget.YearListCosts"

	listCost, err := b.informer.SelectYearListCosts(ctx, userID, year)
	if err != nil {
		if errors.Is(err, storage.ErrRecordNotFound) {
			return nil, ErrRecordNotFound
		}
		b.log.Error(oper, sl.Err(err))
		return nil, fmt.Errorf("%s: %w", oper, err)
	}

	return listCost, nil
}
