package config

import (
	"time"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
)

type Config struct {
	Env  string `yaml:"env" env-required:"true"`
	DB   db     `yaml:"db" env-required:"true"`
	GRPC gRPC   `yaml:"grpc" env-required:"true"`
}

type gRPC struct {
	Port    int           `yaml:"port" env-required:"true"`
	Timeout time.Duration `yaml:"timeout" env-required:"true"`
}

type db struct {
	Host     string `yaml:"host" env-required:"true"`
	Port     string `yaml:"port" env-required:"true"`
	User     string `yaml:"user" env-required:"true"`
	Name     string `yaml:"name" env-required:"true"`
	Password string `env:"POSTGRES_PASSWORD"`
}

var cfg Config

func MustLoad() *Config {
	err := cleanenv.ReadConfig("./config/prod.yaml", &cfg)
	if err != nil {
		panic(err)
	}

	err = godotenv.Load()
	if err != nil {
		panic(err)
	}

	return &cfg
}
