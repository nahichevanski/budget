package models

import "time"

type Cost struct {
	ID       int64
	UserID   int64
	Amount   float32
	Category string
	Comment  string
	Date     time.Time
}
