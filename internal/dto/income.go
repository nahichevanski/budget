package dto

import "time"

type IncomeRequest struct {
	UserID    int64     `json:"user_id"`
	Amount    float64   `json:"amount"`
	Category  string    `json:"category"`
	CreatedAt time.Time `json:"created_at"`
	Comment   string    `json:"comment"`
}

type IncomeView struct {
	ID       int64   `json:"id"`
	Amount   float64 `json:"amount"`
	Category string  `json:"category"`
	Comment  string  `json:"comment"`
}

type SumIncomes struct {
	Amount   float64 `json:"amount"`
	Category string  `json:"category"`
}
