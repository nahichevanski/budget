package dto

import "time"

type CostRequest struct {
	UserID    int64     `json:"user_id"`
	Amount    float64   `json:"amount"`
	Category  string    `json:"category"`
	Comment   string    `json:"comment"`
	CreatedAt time.Time `json:"created_at"`
}

type CostView struct {
	ID       int64   `json:"id"`
	Amount   float64 `json:"amount"`
	Category string  `json:"category"`
	Comment  string  `json:"comment"`
}

type SumCosts struct {
	Amount   float64 `json:"amount"`
	Category string  `json:"category"`
}
