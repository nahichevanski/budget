CREATE TABLE IF NOT EXISTS cost_categories (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS income_categories (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS costs (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    category_id INTEGER NOT NULL,
    FOREIGN KEY  (category_id) REFERENCES cost_categories(id) ON DELETE CASCADE,
    amount NUMERIC(14,2) NOT NULL,
    created_at DATE NOT NULL,
    comment VARCHAR(255)
);


INSERT INTO cost_categories (name)
VALUES ('Продукты'),
       ('ЖКХ'),
       ('Одежда/Обувь'),
       ('Медицина'),
       ('Транспорт'),
       ('Дом'),
       ('Связь'),
       ('Досуг'),
       ('Дети'),
       ('Налоги'),
       ('Гигиена/Косметика'),
       ('Прочее');

CREATE TABLE IF NOT EXISTS incomes (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    category_id INTEGER NOT NULL,
    FOREIGN KEY  (category_id) REFERENCES income_categories(id) ON DELETE CASCADE,
    amount NUMERIC(14,2) NOT NULL,
    created_at DATE NOT NULL,
    comment VARCHAR(255)
);

INSERT INTO income_categories (name)
VALUES ('ЗП'),
       ('Бизнес'),
       ('Инвестиции'),
       ('Пособия'),
       ('Вклад'),
       ('Аренда'),
       ('Подарки'),
       ('Пенсия'),
       ('Алименты'),
       ('Прочее');