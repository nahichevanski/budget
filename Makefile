run: ## Starting application
run:
	go run ./cmd/app/main.go

build: ## Building application
build:
	go build ./cmd/app/main.go

migrate-up: ## Apply up migrations
migrate-up:
	go run ./cmd/migrations/up/up.go

migrate-down: ## Revert down migrations
migrate-down:
	go run ./cmd/migrations/down/down.go